#!/usr/bin/python

import PySide
import PySide.QtGui
import sys

app = PySide.QtGui.QApplication(sys.argv)

from QtMobility import Location

manager = Location.QLandmarkManager()

usage = '''Help:
categories                  Print category names
locations "Category name"   Print locations (name longitude latitude)
add "Category name" "Location 1|25.585224 61.425888" "Location 2|..." Add one or more locations
'''

def find_category(name):
    return [cat for cat in manager.categories() if cat.name() == name][0]

def print_categories():
    for category in manager.categories():
        print category.name()

def print_locations(category):
    f = Location.QLandmarkCategoryFilter()
    f.setCategoryId(category.categoryId())
    landmarks = manager.landmarks(f, -1, 0)
    for landmark in landmarks:
        coord = landmark.coordinate()
        name = landmark.name()
        print u'"%s" %s %s' % (name, repr(coord.longitude()), repr(coord.latitude()))

def add(category, locations):
    for location in locations:
        name, coords = location.split('|')
        lon, lat = [float(c) for c in coords.split()]
        gc = Location.QGeoCoordinate()
        gc.setLongitude(lon)
        gc.setLatitude(lat)
        if not gc.isValid():
            print '* Not valid coordinate for', name
        lm = Location.QLandmark()
        lm.setName(name)
        lm.setCoordinate(gc)
        lm.addCategoryId(category.categoryId())
        ok = manager.saveLandmark(lm)
        if not ok:
            print '* Saving', name, 'failed'

args = sys.argv[1:]
if __name__ == '__main__':
    if len(args) == 0:
        print usage
    else:
        cmd = args[0]
        params = args[1:]
        if cmd == 'categories':
            print_categories()
        elif cmd == 'locations':
            print_locations(find_category(params[0]))
        elif cmd == 'add':
            add(find_category(params[0]), params[1:])
        else:
            print usage
